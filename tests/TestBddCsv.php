<?php

	require("../errorcode.php");
	require("../bddcsv.php");

	class TestBddCsv extends PHPUnit_Framework_TestCase {
		private $_start = false;
		
		public function setUp() {}
		
		public function tearDown() {}
		
		public function testAddUser() {
			$this->assertTrue(addUser("test", "test") == NO_ERRORS);
			$this->assertTrue(addUser("test", "test") == USER_ALREADY_EXISTS);
			$this->assertTrue(addUser("test2", "test") == NO_ERRORS);
		}
		
		public function testGetUserInfos() {
			$this->assertTrue(is_array(getUserInfos("test")));
			$this->assertTrue(getUserInfos("test3") == UNKNOWN_USER);
		}
		
		public function testGetUsersList() {
			$this->assertTrue(getUsersList() != FILE_NOT_FOUND);
		}
		
		public function testModifyUser() {
			$this->assertTrue(modifyUser(array("test", "test2")) == NO_ERRORS);
			$this->assertTrue(modifyUser(array(CSV_PWD_USER_FIELD => "test2")) == INVALID_DATAS);
			$this->assertTrue(modifyUser(array("test3")) == UNKNOWN_USER);
		}
		
		public function testRemoveUser() {
			$this->assertTrue(removeUser("test2") == NO_ERRORS);
			$this->assertTrue(removeUser("test3") == UNKNOWN_USER);
		}
		
		public function testAddTask() {
			$this->assertTrue(addTask("test", "TODO", array("test")) == NO_ERRORS);
			$this->assertTrue(addTask("test", "DONE", array("test3", "test2")) == UNKNOWN_USER_IN_LIST);
		}
		
		public function testModifyTask() {
			$this->assertTrue(modifyTask(array("0", CSV_LOGINS_LIST_TASK_FIELD => array("test"))) == NO_ERRORS);
			$this->assertTrue(modifyTask(array("0", CSV_LOGINS_LIST_TASK_FIELD => array("test3", "test2"))) == UNKNOWN_USER_IN_LIST);
			$this->assertTrue(modifyTask(array("1000000", CSV_LOGINS_LIST_TASK_FIELD => array("test"))) == UNKNOWN_TASK);
			$this->assertTrue(modifyTask(array(CSV_LOGINS_LIST_TASK_FIELD => array("test"))) == INVALID_DATAS);
		}
		
		public function testRemoveTask() {
			$this->assertTrue(removeTask("0") == NO_ERRORS);
			$this->assertTrue(removeTask("1000000") == UNKNOWN_TASK);
		}
		
		public function testModifyFiles() {
			$this->assertTrue(modifyUserFile(array()) == NO_ERRORS);
			$this->assertTrue(modifyTaskFile(array()) == NO_ERRORS);
		}
	}
