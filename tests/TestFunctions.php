<?php

	require("../functions.php");
	
	initSession();

	class TestFunctions extends PHPUnit_Framework_TestCase {
		private $_session;
		
		public function setUp() {}
		
		public function tearDown() {}
		
		public function testStart() {
			$this->_session = (isset($_SESSION['lang']) || !empty($_SESSION['lang'])) ? $_SESSION['lang'] : false;
			unset($_SESSION['lang']);
		}
		
		public function testSecuriseString() {
			$this->assertTrue(substr(securiseString(" chaine"), 0, 1) != " ");
			$this->assertTrue(substr(securiseString("chaine "), -1) != " ");
			$this->assertTrue(substr(securiseString("'"), -1) != "'");
		}
		
		public function testGetPathLanguage() {
			$this->assertTrue(getPathLanguage("en") == '/home/taleia/Sites Web/taskmanager/langs/en.php');
		}
		
		public function testGetLanguage() {
			$this->assertTrue(getLanguage() == "en");
			$_SESSION['lang'] = 'fr';
			$this->assertTrue(getLanguage() == 'fr');
			$_GET['lang'] = 'it';
			$this->assertTrue(getLanguage() == 'fr');
			unset($_SESSION['lang']);
			$this->assertTrue(getLanguage() == "en");
			if($this->_session !== false)
				$_SESSION['lang'] = $this->_session;
		}
	}
