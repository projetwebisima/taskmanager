<?php

	require("../urlpath.php");

	class TestUrlPath extends PHPUnit_Framework_TestCase {
		public function setUp() {}
		
		public function tearDown() {}
		
		public function testGetUrl() {
			$this->assertTrue(getUrl() == 'http://localhost/taskmanager');
			$this->assertTrue(getUrl("test.php") == 'http://localhost/taskmanager/test.php');
			$this->assertTrue(getUrl("/test.php") == 'http://localhost/taskmanager/test.php');
		}
		
		public function testGetPath() {
			$this->assertTrue(getPath() == '/home/taleia/Sites Web/taskmanager');
			$this->assertTrue(getPath("test.php") == '/home/taleia/Sites Web/taskmanager/test.php');
			$this->assertTrue(getPath("/test.php") == '/home/taleia/Sites Web/taskmanager/test.php');
		}
	}
