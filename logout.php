<?php
require(__DIR__ . '/include.php');

unset($_SESSION['auth']);
unset($_SESSION['login']);

header('Location: ' . getUrl('index.php'));
exit();