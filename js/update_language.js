$(function() {
	$("#lg").change(function() {
		var url = location.href;
		if(/\?.*lang=/.test(url)) {
			url = url.replace(/(\?.*lang=)[^&]*/, "$1" + $(this).children("option:selected").first().val())
		} else url += ((/\?/.test(url)) ? "&" : "?") + "lang=" + $(this).children("option:selected").first().val();
		location.href = url;
	});
});