$(function() {
	$(".adduser").click(function() {
		$newLine = "<tr><td><input type=\"text\"/></td><td><input type=\"password\"/></td>";
		$newLine += "<td><input type=\"checkbox\"/></td><td>"
		$newLine += "<input type=\"button\" class=\"deluser_new\" value=\"" + LABEL_REMOVAL_BUTTONS +"\"/></td></tr>";
		$(".userinfos").append($newLine);
		$(".deluser_new").click(function() {
			delUser($(this));
		});
		$(".deluser_new").addClass("deluser");
		$(".deluser_new").addClass("deluser_new");
	});
	
	$(".deluser").click(function() {
		delUser($(this));
	});
	
	$(".save").click(function() {
		$datasUsers = new Array();
		$(".userinfos").find("tr").each(function() {
			var $datasUser = new Array(
				$(this).find("td:first-child > input").val(),
				$(this).find("td:nth-child(2) > input").val(),
				($(this).find("td:nth-child(3) > input").is(':checked')) ? "true" : "false"
			);
			console.log($(this).find("td:nth-child(3) > input").val());
			$datasUsers.push($datasUser);
		});
		$.post("save_users.php", {"data": $datasUsers }).done(function(msg) {
			location.href = "mainpage.php";
		});
	});
});

function delUser($elem) {
	$elem.unbind("click");
	$elem.parent().parent().remove();
}
