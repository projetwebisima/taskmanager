<?php
	require(__DIR__ . '/include.php');
	
	if(isset($_POST["data"])) {
		$usersInfosList = getUsersInfosList();
		$usersDatas = array();
		foreach($_POST["data"] as $value) {
			if(isset($value[0]) && !empty($value[0])) {
				$login = securiseString($value[0]);
				$password = securiseString($value[1]);
				if(!empty($password))
					$password = hash('sha512', $password);
				$alreadyConnected = false;
				$roleUser = securiseString($value[2]);
				foreach($usersInfosList as $userInfos)
					if($userInfos[CSV_LOGIN_USER_FIELD] == $login) {
						if(empty($password))
							$password = $userInfos[CSV_PWD_USER_FIELD];
						$alreadyConnected = $userInfos[CSV_STATE_USER_FIELD];
						break;
					}
				array_push($usersDatas, array($login, $password, $alreadyConnected, $roleUser));
			}
		}
		modifyUserFile($usersDatas);
	}
	
	exit();