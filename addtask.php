<?php
	require(__DIR__ . '/include.php');
	if(!isset($_SESSION['auth'])){
		header("Location: " . getUrl("login.php"));	
		exit();
	}
?>

<!DOCTYPE html>
<html style="height: 100%">
	<head>
		<title><?php echo TITLE; ?></title>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="admin.css" type="text/css" /> 
	</head>
	<body style="background-image: url('bg_mainpage.png'); background-size: 100% 100%;">
	<div style="width: 100%; text-align: right; margin: 10px;">
			<a href="<?php echo getUrl('mainpage.php'); ?>" style="margin-right: 10px;"><button><?php echo LABEL_TASKS_MANAGEMENT; ?></button></a>
			<a href="<?php echo getUrl('logout.php'); ?>" style="margin-right: 10px;"><button><?php echo LABEL_LOGOUT; ?></button></a>
			<select id="lg" style="margin-right: 10px;">
				<option value="en" <?php if(getLanguage() == "en") { ?>selected<?php } ?>>English</option>
				<option value="fr" <?php if(getLanguage() == "fr") { ?>selected<?php } ?>>Français</option>
			</select> 
		</div>

<div class="container">
  <h2><?php echo LABEL_TASK_CREATE; ?></h2>
<form action="add.php" method="post"> 
    <div class="form-group">
      <label for="usr"><?php echo LABEL_TASK_NAME; ?></label>
      <input type="text" class="form-control" id="usr" name="nom">
    </div>

	  <div class="form-group">
<INPUT name="Go" type="submit" Value="Go" width=150> 
    </div>
	
  </form>
</div>
<script src="<?php echo getUrl("js/libs/jquery.js"); ?>"></script>
<script src="<?php echo getUrl("js/update_language.js"); ?>"></script>
</body>
</html>