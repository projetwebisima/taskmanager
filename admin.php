<?php
	require(__DIR__ . '/include.php');
	if(!isset($_SESSION['auth'])) {
		header("Location: " . getUrl("mainpage.php"));	
		exit();
	} else {
		$userInfos = getUserInfos($_SESSION['login']);
		if($userInfos[CSV_ROLE_USER_FIELD] != "true"){
			header("Location: " . getUrl("mainpage.php"));	
			exit();
		}
	}
	
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo TITLE; ?></title>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="admin.css" type="text/css" /> 
	</head>
	<body>
		<div style="width: 100%; text-align: right; margin: 10px;">
			<a href="<?php echo getUrl('mainpage.php'); ?>" style="margin-right: 10px;"><button><?php echo LABEL_TASKS_MANAGEMENT; ?></button></a>
			<a href="<?php echo getUrl('logout.php'); ?>" style="margin-right: 10px;"><button><?php echo LABEL_LOGOUT; ?></button></a>
			<select id="lg" style="margin-right: 10px;">
				<option value="en" <?php if(getLanguage() == "en") { ?>selected<?php } ?>>English</option>
				<option value="fr" <?php if(getLanguage() == "fr") { ?>selected<?php } ?>>Français</option>
			</select> 
		</div>
		
		<form action="" method="POST">
			<table>
				<thead>
					<tr>
						<th><?php echo LABEL_LOGIN_COLUMN; ?></th>
						<th><?php echo LABEL_PASSWORD_COLUMN; ?></th>
						<th><?php echo LABEL_USER_ROLE_COLUMN; ?></th>
						<th></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th><?php echo LABEL_LOGIN_COLUMN; ?></th>
						<th><?php echo LABEL_PASSWORD_COLUMN; ?></th>
						<th><?php echo LABEL_USER_ROLE_COLUMN; ?></th>
						<th></th>
					</tr>
				</tfoot>
				<tbody class="userinfos">
					<?php 
						$userInfosList = getUsersInfosList();
						uasort($userInfosList, function($a, $b) {
						    if ($a[CSV_LOGIN_USER_FIELD] == $b[CSV_LOGIN_USER_FIELD]) {
						        return 0;
						    }
						    return ($a[CSV_LOGIN_USER_FIELD] < $b[CSV_LOGIN_USER_FIELD]) ? -1 : 1;
						});
						foreach($userInfosList as $value) { ?>
					<tr>
						<td>
							<input type="text" value="<?php echo $value[CSV_LOGIN_USER_FIELD]; ?>"/>
						</td>
						<td>
							<input type="password" placeholder="<?php echo VALUE_PASSWORD_FIELDS; ?>"/>
						</td>
						<td>
							<input type="checkbox" <?php if($value[CSV_ROLE_USER_FIELD] == "true") { ?>checked<?php } ?>/>
						</td>
						<td>
							<input type="button" class="deluser" value="<?php echo LABEL_REMOVAL_BUTTONS; ?>"/>
						</td>
					</tr>
						<?php }
					?>
				</tbody>
			</table>
			<input type="button" class="adduser" value="<?php echo LABEL_USER_ADD_BUTTON; ?>"/>
			<input type="button" class="save" value="<?php echo LABEL_SAVE_CHANGES_BUTTON; ?>"/>
		</form>
		<script>
			var LABEL_REMOVAL_BUTTONS = "<?php echo LABEL_REMOVAL_BUTTONS; ?>";
		</script>
		<script src="<?php echo getUrl("js/libs/jquery.js"); ?>"></script>
		<script src="<?php echo getUrl("js/update_language.js"); ?>"></script>
		<script src="<?php echo getUrl("js/user_management.js"); ?>"></script>
	</body>
</html>
