<?php
	
	function getUrl($urlRelative = '') {
		$urlHome = ''; //Mettre l'adresse url du site.
		if($urlRelative == '')
			return $urlHome;
		if(substr($urlRelative, 0, 1) != '/')
			$urlHome .= '/';
		return $urlHome . $urlRelative;
	}
	
	function getPath($pathRelative = '') {
		$pathHome ='';
		if($pathRelative == '') //Mettre l'adresse du dossier du site à partir de la racine du serveur.
			return $pathHome;
		if(substr($pathRelative, 0, 1) != '/')
			$pathHome .= '/';
		return $pathHome . $pathRelative;
	}