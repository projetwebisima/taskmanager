<?php
require(__DIR__ . '/include.php');

if(isset($_POST['login']) && !empty($_POST['login']) && isset($_POST['password']) && !empty($_POST['password'])) {
	$login = $_POST['login'];
	$password = $_POST['password'];

	$tryuser=getUserInfos($login);
	if(is_array($tryuser) && $tryuser[1]==hash('sha512', $password)) {
		initSession();
		$_SESSION['auth']=true;
		$_SESSION['login']=$login;
		setcookie ("login", $login, time() + 31536000);
		header('Location: ' . getUrl('mainpage.php'));
		exit();
	}
	$tryuser = WRONG_PASS;
}

header('Location: ' . getUrl('index.php') . "?error=" . $tryuser);
exit();

?>
