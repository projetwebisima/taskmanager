<?php
	/** The title of the website in french.
	 * @var string
	 */
	const TITLE = "ZZ Task";

	/** The welcome message of the website in french.
	 * @var string
	 */
	const WELCOME_MESSAGE = "Bienvenue sur le gestionnaire de tâche !";

	/** The login field label in french.
	 * @var string
	 */
	const LABEL_LOGIN = "Votre identifiant :";

	/** The password field label in french.
	 * @var string
	 */
	const LABEL_PASSWORD = "Votre mot de passe :";

	/** The connection button label in french.
	 * @var string
	 */
	const LABEL_SIGN_IN = "Connexion";
	
	/** The label of the login column of the user management table in french.
	 * @var string
	 */
	const LABEL_LOGIN_COLUMN = "Identifiant";
	
	/** The label of the password column of the user management table in french.
	 * @var string
	 */
	const LABEL_PASSWORD_COLUMN = "Mot de passe";

	/** The label of the user role column of the user management table in french.
	 * @var string
	 */
	const LABEL_USER_ROLE_COLUMN = "Admin";
	
	/** The value of the password fields of the user management table in french.
	 * @var string
	 */
	const VALUE_PASSWORD_FIELDS = "Mot de passe inchangé";
	
	/** The label of the removal buttons of the user management table in french.
	 * @var string
	 */
	const LABEL_REMOVAL_BUTTONS = "Supprimer";
	
	/** The label of the button to add a user into the user management table in french.
	 * @var string
	 */
	const LABEL_USER_ADD_BUTTON = "Ajouter un utilisateur";
	
	/** The label of the button to save changes of the user management table in french.
	 * @var string
	 */
	const LABEL_SAVE_CHANGES_BUTTON = "Valider les changements";
	
		
	/** label of the column to do
	 * @var string
	 */
	const LABEL_TODO = "A faire";
	
	/** label of the column WIP
	 * @var string
	 */
	const LABEL_WIP = "En cours";
	
	/** label of the column DONE
	 * @var string
	 */
	const LABEL_DONE = "Fini";
	
		/** label of the loging error
	 * @var string
	 */
	const LABEL_LOGERROR = "Mauvaise combinaison Identifiant/Mot de passe";
	
	/** Label for the users management link in french.
	 * @var string
	 */
	const LABEL_USERS_MANAGEMENT = "Gestion des utilisateurs";
	
	/** Label for the tasks management link in french.
	 * @var string
	 */
	const LABEL_TASKS_MANAGEMENT = "Gestion des tâches";
	
	/** Label for the logout link in french.
	 * @var string
	 */
	const LABEL_LOGOUT = "Déconnexion";
	
	/** Label for the task create form in french.
	 * @var string
	 */
	const LABEL_TASK_CREATE = "Créer votre tâche";
	
	/** Label for the task edit form in french.
	 * @var string
	 */
	const LABEL_TASK_EDIT = "Edition";
	
	/** Label for the task name input in french.
	 * @var string
	 */
	const LABEL_TASK_NAME = "Nom";
	
	/** Label for the task name input in french.
	 * @var string
	 */
	const LABEL_TASK_USER = "Utilisateur";
	
	/** Label for the task status input in french.
	 * @var string
	 */
	const LABEL_TASK_STATUS = "Statut";