<?php
	/** The title of the website in english.
	 * @var string
	 */
	const TITLE = "ZZ Task";

	/** The welcome message of the website in english.
	 * @var string
	 */
	const WELCOME_MESSAGE = "Welcome to the task manager !";

	/** The login field label in english.
	 * @var string
	 */
	const LABEL_LOGIN = "Your login :";

	/** The password field label in english.
	 * @var string
	 */
	const LABEL_PASSWORD = "Your password :";

	/** The connection button label in english.
	 * @var string
	 */
	const LABEL_SIGN_IN = "Sign in";
	
	/** The label of the login column of the user management table in english.
	 * @var string
	 */
	const LABEL_LOGIN_COLUMN = "Login";
	
	/** The label of the password column of the user management table in english.
	 * @var string
	 */
	const LABEL_PASSWORD_COLUMN = "Password";

	/** The label of the user role column of the user management table in english.
	 * @var string
	 */
	const LABEL_USER_ROLE_COLUMN = "Admin";
	
	/** The value of the password fields of the user management table in english.
	 * @var string
	 */
	const VALUE_PASSWORD_FIELDS = "Unchanged password";
	
	/** The label of the removal buttons of the user management table in english.
	 * @var string
	 */
	const LABEL_REMOVAL_BUTTONS = "Remove";
	
	/** The label of the button to add a user into the user management table in english.
	 * @var string
	 */
	const LABEL_USER_ADD_BUTTON = "Add a user";
	
	/** The label of the button to save changes of the user management table in english.
	 * @var string
	 */
	const LABEL_SAVE_CHANGES_BUTTON = "Save changes";
	
	/** label of the column to do
	 * @var string
	 */
	const LABEL_TODO = "To do";
	
		
	/** label of the column WIP
	 * @var string
	 */
	const LABEL_WIP = "Work in progress";
		
	/** label of the column DONE
	 * @var string
	 */
	const LABEL_DONE = "Done";

	/** label of the loging error
	 * @var string
	 */
	const LABEL_LOGERROR = "Wrong combination User/Password";
	
	/** Label for the users management link in english.
	 * @var string
	 */
	const LABEL_USERS_MANAGEMENT = "Users management";
	
	/** Label for the tasks management link in french.
	 * @var string
	 */
	const LABEL_TASKS_MANAGEMENT = "Tasks management";
	
	/** Label for the logout link in english.
	 * @var string
	 */
	const LABEL_LOGOUT = "Log out";
	
	/** Label for the task create form in french.
	 * @var string
	 */
	const LABEL_TASK_CREATE = "Create your task";
	
	/** Label for the task edition form in english.
	 * @var string
	 */
	const LABEL_TASK_EDIT = "Edit";
	
	/** Label for the task name input in english.
	 * @var string
	 */
	const LABEL_TASK_NAME = "Name";
	
	/** Label for the task name input in english.
	 * @var string
	 */
	const LABEL_TASK_USER = "User";
	
	/** Label for the task status input in english.
	 * @var string
	 */
	const LABEL_TASK_STATUS = "Status";