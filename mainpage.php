<?php
	require(__DIR__ . '/include.php');
	if(!isset($_SESSION['auth'])){
		header("Location: " . getUrl("index.php"));	
		exit();
	}

	$tasklist=getTasksList();

?>
<html style="height: 100%">
	<head>
	<title><?php echo TITLE; ?></title>
	<meta charset="UTF-8"/>
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	</head>




<body style="background-image: url('bg_mainpage.png'); background-size: 100% 100%;">

			<h2 class="titre" style="text-align: center;"><?php echo TITLE; ?></h2>
<div style="width: 100%; text-align: right; margin: 10px 0;">
	<?php $userInfos = getUserInfos($_SESSION['login']); if($userInfos[CSV_ROLE_USER_FIELD] == "true") { ?>
		<a href="<?php echo getUrl('admin.php'); ?>" style="margin-right: 10px;"><button><?php echo LABEL_USERS_MANAGEMENT; ?></button></a>
	<?php } ?>
	<a href="<?php echo getUrl('logout.php'); ?>" style="margin-right: 10px;"><button><?php echo LABEL_LOGOUT; ?></button></a>
	<select id="lg" style="margin-right: 10px;">
		<option value="en" <?php if(getLanguage() == "en") { ?>selected<?php } ?>>English</option>
		<option value="fr" <?php if(getLanguage() == "fr") { ?>selected<?php } ?>>Français</option>
	</select>
</div>
<?php  $iterator=0; 
/*boucle sur les chaque tache du fichier csv et l'affiche*/ ?>
<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="panel panel-default">
				  <div class="panel-heading"><?php echo LABEL_TODO; ?>
				  <div class="pull-right"  role="group" aria-label="...">
											<form action="addtask.php" id="add" method="post" style="display: none;">
											</form>
											<a href="javascript:;" onclick="javascript: document.getElementById('add').submit()"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a> 
						</div>
</div>
				  <div class="panel-body">
							  <?php foreach($tasklist as $value) { 
						  $iterator=$iterator+3;
						  if(strtoupper($value[2])=="TODO"){
						  ?>
						  <div class="panel panel-default">
						  <div class="panel-heading"><?php echo $value[1]; ?></div>
					  <div class="panel-body">
						<?php echo $value[3]; ?>
						<?php echo "  "; ?>			
						<div class="pull-right"  role="group" aria-label="...">
											<form action="edittask.php" id="<?php  echo $iterator+2; ?>" method="get" style="display: none;">
											<input type="text" name="tid" value= <?php echo $value[0]; ?> />
											</form>
											<a href="javascript:;" onclick="javascript: document.getElementById('<?php  echo $iterator+2;  ?>').submit()"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
											
											
											<form action="deletestuff.php" id="<?php  echo $iterator;  ?>" method="post" style="display: none;">
											<input type="text" name="tid" value= <?php echo $value[0]; ?> />
											</form>
											<a href="javascript:;" onclick="javascript: document.getElementById('<?php  echo $iterator;  ?>').submit()"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a> 
										 
										 
											<form action="todowip.php" id="<?php  echo $iterator+1; ?>" method="post" style="display: none;">
											<input type="text" name="tid" value= <?php echo $value[0]; ?> />
											</form>
											<a href="javascript:;" onclick="javascript: document.getElementById('<?php  echo $iterator+1;  ?>').submit()"><span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span></a> 


						</div>
					  </div>
					</div>
					  <?php } } ?>
				  </div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
				  <div class="panel-heading"><?php echo LABEL_WIP; ?></div>
				  <div class="panel-body">
							  <?php foreach($tasklist as $value) { 
						  $iterator=$iterator+4 ;
						  if(strtoupper($value[2])=="IN PROGRESS"){
						  ?>
						  <div class="panel panel-default">
						  <div class="panel-heading"><?php echo $value[1]; ?></div>
					  <div class="panel-body">
						<?php echo $value[3]; ?>
						<?php echo "  "; ?>			
						<div class="pull-right"  role="group" aria-label="...">
							<form action="wiptodo.php" id="<?php  echo $iterator ; ?>" method="post" style="display: none;">
							<input type="text" name="tid" value= <?php echo $value[0]; ?> />
							</form>	
							<a href="javascript:;" onclick="javascript: document.getElementById('<?php  echo $iterator;  ?>').submit()"><span class="glyphicon glyphicon-triangle-left" aria-hidden="true"></span></a> 
							
							<form action="edittask.php" id="<?php  echo $iterator+3; ?>" method="get" style="display: none;">
							<input type="text" name="tid" value= <?php echo $value[0]; ?> />
							</form>
							<a href="javascript:;" onclick="javascript: document.getElementById('<?php  echo $iterator+3;  ?>').submit()"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
				
							<form action="deletestuff.php" id="<?php  echo $iterator+1;  ?>" method="post" style="display: none;">
							<input type="text" name="tid" value= <?php echo $value[0]; ?> />
							</form>					
							<a href="javascript:;" onclick="javascript: document.getElementById('<?php  echo $iterator+1;  ?>').submit()"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a> 
							
							
							<form action="wipdone.php" id="<?php  echo $iterator+2; ?>" method="post" style="display: none;">
							<input type="text" name="tid" value= <?php echo $value[0]; ?> />
							</form>
							<a href="javascript:;" onclick="javascript: document.getElementById('<?php  echo $iterator+2;  ?>').submit()"><span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span></a>
						</div>
					  </div>
					</div>
					  <?php } } ?>
				  </div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
				  <div class="panel-heading"><?php echo LABEL_DONE; ?></div>
				  <div class="panel-body">
							  <?php foreach($tasklist as $value) {
						  $iterator=$iterator+4 ;								  
						  if(strtoupper($value[2])=="DONE"){
						  ?>
						  <div class="panel panel-default">
						  <div class="panel-heading"><?php echo $value[1]; ?></div>
					  <div class="panel-body">
						<?php echo $value[3]; ?>
						<?php echo "  "; ?>			
						<div class="pull-right"  role="group" aria-label="...">
							<form action="donewip.php" id="<?php  echo $iterator ; ?>" method="post" style="display: none;">
							<input type="text" name="tid" value= <?php echo $value[0]; ?> />
							</form>
							<a href="javascript:;" onclick="javascript: document.getElementById('<?php  echo $iterator;  ?>').submit()"><span class="glyphicon glyphicon-triangle-left" aria-hidden="true"></span></a> 
							
							
							<form action="edittask.php" id="<?php  echo $iterator+2; ?>" method="get" style="display: none;">
							<input type="text" name="tid" value= <?php echo $value[0]; ?> />
							</form>
							<a href="javascript:;" onclick="javascript: document.getElementById('<?php  echo $iterator+2;  ?>').submit()"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
							
							
							<form action="deletestuff.php" id="<?php  echo $iterator+1;  ?>" method="post" style="display: none;">
							<input type="text" name="tid" value= <?php echo $value[0]; ?> />
							</form>
							<a href="javascript:;" onclick="javascript: document.getElementById('<?php  echo $iterator+1;  ?>').submit()"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a> 
						</div>
					  </div>
					</div>
					  <?php } } ?>
				  </div>
				</div>
			</div>
		</div>
	</div>
<script src="<?php echo getUrl("js/libs/jquery.js"); ?>"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo getUrl("js/update_language.js"); ?>"></script>
<script src="<?php echo getUrl("js/task_managment.js"); ?>"></script>
</body>
</html>

