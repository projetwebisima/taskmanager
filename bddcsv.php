<?php

	/** File containing users datas.
	 * @var string
	 */
	define("CSV_USERS_FILE", "users.csv");
	
	/** User login field index.
	 * @var int
	 */
	define("CSV_LOGIN_USER_FIELD", "0");
	
	/** User password field index.
	 * @var int
	 */
	define("CSV_PWD_USER_FIELD", "1");
	
	/** User connection state field index.
	 * @var int
	 */
	define("CSV_STATE_USER_FIELD", "2");
	
	/** Field verifying the user role.
	 * @var int
	 */
	define("CSV_ROLE_USER_FIELD", "3");

	/** File containing tasks datas.
	 * @var string
	 */
	define("CSV_TASKS_FILE", "tasks.csv");
	
	/** Task id field index.
	 * @var int
	 */
	define("CSV_ID_TASK_FIELD", "0");
	
	/** Task name field index.
	 * @var int
	 */
	define("CSV_NAME_TASK_FIELD", "1");
	
	/** Task state field index.
	 * @var int
	 */
	define("CSV_STATE_TASK_FIELD", "2");
	
	/** Task participants logins list field index.
	 * @var int
	 */
	define("CSV_LOGINS_LIST_TASK_FIELD", "3");

	/** Add a user to the datas file.
	 * @param string $login User login.
	 * @param string $pwd User password.
	 * @return int The error code.
	 */
	function addUser($login, $pwd) {
		if(is_array(getUserInfos($login)))
			return USER_ALREADY_EXISTS;
		if(($file = fopen(CSV_USERS_FILE, 'a')) !== false) {
			$hashedPwd = hash('sha512', $pwd);
			$userDatas = array($login, $hashedPwd, 'false', 'false');
			fputcsv($file, $userDatas, ';');
			fclose($file);
			return NO_ERRORS;
		} else return FILE_NOT_FOUND;
	}
	
	/** Return the user datas.
	 * @param string $login
	 * @return mixed The user datas or an error code.
	 */
	function getUserInfos($login) {
		if(($file = fopen(CSV_USERS_FILE, 'r')) !== false) {
			$userDatas = array();
			while(empty($userDatas) && ($datas = fgetcsv($file, 4096, ';')) !== false)
				if($datas[0] == $login)
					$userDatas = $datas;
			fclose($file);
			if(empty($userDatas))
				return UNKNOWN_USER;
			return $userDatas;
		} else return FILE_NOT_FOUND;
	}
	
	/** Return the users datas list.
	 * @return mixed The users datas list or an error code.
	 */
	function getUsersInfosList() {
		if(($file = fopen(CSV_USERS_FILE, 'r')) !== false) {
			$userDatas = array();
			while(($datas = fgetcsv($file, 4096, ';')) !== false)
				array_push($userDatas, $datas);
			fclose($file);
			return $userDatas;
		} else return FILE_NOT_FOUND;
	}
	
	/** Return the users list.
	 * @return mixed The users list or an error code.
	 */
	function getUsersList() {
		if(($file = fopen(CSV_USERS_FILE, 'r')) !== false) {
			$userDatas = array();
			while(($datas = fgetcsv($file, 4096, ';')) !== false)
				array_push($userDatas, $datas[0]);
			fclose($file);
			return $userDatas;
		} else return FILE_NOT_FOUND;
	}
	
	/** Rewrite the users file with the new users datas.
	 * @param array $usersDatas The users datas.
	 * @return string The error code.
	 */
	function modifyUserFile($usersDatas) {
		if(($file = fopen(CSV_USERS_FILE, 'w')) !== false) {
			ftruncate($file, 0);
			foreach($usersDatas as $value)
				fputcsv($file, $value, ';');
			fclose($file);
			return NO_ERRORS;
		} else return FILE_NOT_FOUND;
	}
	
	/** Modify the user datas.
	 * @param array $userInfos The new user datas. The <code>CSV_LOGIN_USER_FIELD</code> index must be precised.
	 * @return int The error code.
	 */
	function modifyUser($userInfos) {
		if(!isset($userInfos[CSV_LOGIN_USER_FIELD]))
			return INVALID_DATAS;
		$userDatas = getUsersInfosList();
		if(!is_array($userDatas))
			return $userDatas;
		$found = false;
		foreach($userDatas as $key => $value)
			if($value[0] == $userInfos[CSV_LOGIN_USER_FIELD]) {
				$userDatas[$key][0] = $userInfos[CSV_LOGIN_USER_FIELD];
				$userDatas[$key][1] = (isset($userInfos[CSV_PWD_USER_FIELD]) && !empty($userInfos[CSV_PWD_USER_FIELD])) ? 
											hash('sha512', $userInfos[CSV_PWD_USER_FIELD]) : $value[1];
				$userDatas[$key][2] = (isset($userInfos[CSV_STATE_USER_FIELD])
										&& ($userInfos[CSV_STATE_USER_FIELD] == 'true' || $userInfos[CSV_STATE_USER_FIELD] == 'false')) ? 
											$userInfos[CSV_STATE_USER_FIELD] : $value[2];
				$userDatas[$key][3] = (isset($userInfos[CSV_ROLE_USER_FIELD])
										&& ($userInfos[CSV_ROLE_USER_FIELD] == 'true' || $userInfos[CSV_ROLE_USER_FIELD] == 'false')) ? 
											$userInfos[CSV_ROLE_USER_FIELD] : $value[3];
				$found = true;
				break;
			}
		if(!$found)
			return UNKNOWN_USER;
		$errorCode = modifyUserFile($userDatas);
		if($errorCode != NO_ERRORS)
			return $errorCode;
		return NO_ERRORS;
	}

	/** Remove an user.
	 * @param array $login The user to remove.
	 * @return int The error code.
	 */
	function removeUser($login) {
		$userDatas = getUsersInfosList();
		if(!is_array($userDatas))
			return $userDatas;
		$found = false;
		foreach($userDatas as $key => $value)
			if($value[0] == $login) {
				unset($userDatas[$key]);
				$found = true;
				break;
			}
		if(!$found)
			return UNKNOWN_USER;
		$errorCode = modifyUserFile($userDatas);
		if($errorCode != NO_ERRORS)
			return $errorCode;
		return NO_ERRORS;
	}
	
	/** Verify if the users which have their login in the list exists.
	 * @param mixed $loginsList The users login list. Can be a string or an array.
	 * @return mixed An array of the users login list or the error code.
	 */
	function verifyLoginsList($loginsList) {
		if(is_array($loginsList)) {
			$loginsListArray = $loginsList;
			$loginsList = implode(",", $loginsList);
		} else $loginsListArray = explode(",", $loginsList);
		
		foreach($loginsListArray as $value)
			if(getUserInfos($value) == UNKNOWN_USER)
				return UNKNOWN_USER_IN_LIST;
			
		return $loginsList;
	}
	
	/** Add a task to the datas file.
	 * @param string $name The task name.
	 * @param string $state The state in which the task is.
	 * @param array $loginsList The owners logins list of the task.<br>
	 * If one of the logins is invalid or doesn't exist, an error will be returned.
	 * @return int The error code.
	 */
	function addTask($name, $state, $loginsList) {
		$tasksDatas = getTasksList();
		if(!empty($tasksDatas)) {
			$taskDatas = array_pop($tasksDatas);
			$idNext = $taskDatas[CSV_ID_TASK_FIELD] + 1;
		} else $idNext = 0;
		
		$loginsList = verifyLoginsList($loginsList);
		if(is_numeric(verifyLoginsList($loginsList))){
			return $loginsList;
		}
		if(($file = fopen(CSV_TASKS_FILE, 'a+')) !== false) {
			$taskDatas = array($idNext, $name, $state, $loginsList);
			fputcsv($file, $taskDatas, ';');
			fclose($file);
			return NO_ERRORS;
		} else return FILE_NOT_FOUND;
	}
	
	/** Return the tasks list.
	 * @return mixed The tasks list or an error code.
	 */
	function getTasksList() {
		if(($file = fopen(CSV_TASKS_FILE, 'r')) !== false) {
			$userDatas = array();
			while(empty($userTasks) && ($datas = fgetcsv($file, 4096, ';')) !== false)
				array_push($userDatas, $datas);
			fclose($file);
			return $userDatas;
		} else return FILE_NOT_FOUND;
	}
	
	/** Rewrite the tasks file with the new tasks datas.
	 * @param array $tasksDatas The tasks datas.
	 * @return string The error code.
	 */
	function modifyTaskFile($tasksDatas) {
		if(($file = fopen(CSV_TASKS_FILE, 'w')) !== false) {
			ftruncate($file, 0);
			foreach($tasksDatas as $value)
				fputcsv($file, $value, ';');
			fclose($file);
			return NO_ERRORS;
		} else return FILE_NOT_FOUND;
	}
	
	/** Modify the task datas.
	 * @param array $taskInfos The new task datas. The <code>CSV_ID_TASK_FIELD</code> index must be precised.
	 * @return int The error code.
	 */
	function modifyTask($taskInfos) {
		if(!isset($taskInfos[CSV_ID_TASK_FIELD]))
			return INVALID_DATAS;
		$tasksDatas = getTasksList();
		if(!is_array($tasksDatas))
			return $tasksDatas;
		$found = false;
		
		if(isset($taskInfos[CSV_LOGINS_LIST_TASK_FIELD]) && !empty($taskInfos[CSV_LOGINS_LIST_TASK_FIELD])) {
			$taskInfos[CSV_LOGINS_LIST_TASK_FIELD] = verifyLoginsList($taskInfos[CSV_LOGINS_LIST_TASK_FIELD]);
			if(is_numeric($taskInfos[CSV_LOGINS_LIST_TASK_FIELD]))
				return $taskInfos[CSV_LOGINS_LIST_TASK_FIELD];
		}
		
		foreach($tasksDatas as $key => $value)
			if($value[0] == $taskInfos[CSV_ID_TASK_FIELD]) {
				$tasksDatas[$key][0] = $taskInfos[CSV_ID_TASK_FIELD];
				$tasksDatas[$key][1] = (isset($taskInfos[CSV_NAME_TASK_FIELD]) && !empty($taskInfos[CSV_NAME_TASK_FIELD])) ?
					$taskInfos[CSV_NAME_TASK_FIELD] : $value[1];
				$tasksDatas[$key][2] = (isset($taskInfos[CSV_STATE_TASK_FIELD]) && !empty($taskInfos[CSV_STATE_TASK_FIELD])) ?
					$taskInfos[CSV_STATE_TASK_FIELD] : $value[2];
				$tasksDatas[$key][3] = (isset($taskInfos[CSV_LOGINS_LIST_TASK_FIELD]) && !empty($taskInfos[CSV_LOGINS_LIST_TASK_FIELD])) ?
						$taskInfos[CSV_LOGINS_LIST_TASK_FIELD] : $value[3];
				$found = true;
				break;
			}
		if(!$found)
			return UNKNOWN_TASK;
		$errorCode = modifyTaskFile($tasksDatas);
		if($errorCode != NO_ERRORS)
			return $errorCode;
		return NO_ERRORS;
	}
	
	function removeTask($id) {
		$tasksDatas = getTasksList();
		if(!is_array($tasksDatas))
			return $tasksDatas;
		$found = false;
		foreach($tasksDatas as $key => $value)
			if($value[0] == $id) {
				unset($tasksDatas[$key]);
				$found = true;
				break;
			}
		if(!$found)
			return UNKNOWN_TASK;
		$errorCode = modifyTaskFile($tasksDatas);
		if($errorCode != NO_ERRORS)
			return $errorCode;
		return NO_ERRORS;
	}
	
