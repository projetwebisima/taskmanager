<?php

	/** Error code when no errors has been detected.
	 * @var int
	 */
	define("NO_ERRORS", "0");
	
	/** Error code when a file was not found.
	 * @var int
	 */
	define("FILE_NOT_FOUND", "1");
	
	/** Error code when a user is unknown.
	 * @var int
	 */
	define("UNKNOWN_USER", "2");
	
	/** Error code when a user already exists.
	 * @var int
	 */
	define("USER_ALREADY_EXISTS", "3");
	
	/** Error code when datas were invalid.
	 * @var int
	 */
	define("INVALID_DATAS", "4");
	
	/** Error code when a task is unknown.
	 * @var int
	 */
	define("UNKNOWN_TASK", "5");
	
	/** Error code when a user is not in a list of participants of a task.
	 * @var int
	 */
	define("UNKNOWN_USER_IN_LIST", "6");
	
	/** Error code when the typed password is wrong.
	 * @var int
	 */
	define("WRONG_PASS", "7");
	
