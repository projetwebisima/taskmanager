<?php

	require(__DIR__ . "/urlpath.php");

	/** Start session variables securely.
	 */
	function initSession() {
		if(session_id() == "")
			session_start();
		else session_regenerate_id(true);
	}

	/** Securise a string against injection attack.
	 * @param string $string The string to securise.
	 * @return string The string securised.
	 */
	function securiseString($string) {
		if(phpversion() >= "5.4.0")
			return trim(htmlspecialchars($string, ENT_HTML5 | ENT_QUOTES, "UTF-8"));
		return trim(htmlspecialchars($string, ENT_QUOTES, "UTF-8"));
	}
	
	/** Return the relative path of the language file to use.
	 * @param string $lang <code>en</code> for english, <code>fr</code> for french.
	 * @return string The relative path of the language file.
	 */
	function getPathLanguage($lang) {
		return getPath("langs/" . $lang . ".php");
	}

	/** Return the language to use.
	 * @return string The language.<br>
	 * Can be <code>en</code> for english, <code>fr</code> for french.<br>
	 * Default language : <code>en</code>.
	 */
	function getLanguage() {
		if(isset($_GET["lang"]) && !empty($_GET["lang"])) {
			$lang = securiseString($_GET["lang"]);
			if(!in_array($lang, array("en", "fr")) && isset($_SESSION["lang"]) && !empty($_SESSION["lang"]))
				$lang = $_SESSION['lang'];
		} elseif(isset($_SESSION["lang"]) && !empty($_SESSION["lang"]))
			$lang = $_SESSION["lang"];
		else $lang = "en";
		if(!in_array($lang, array("en", "fr")))
			$lang = "en";
		$_SESSION["lang"] = $lang;
		return $lang;
	}
