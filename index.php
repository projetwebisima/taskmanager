<?php
	require(__DIR__ . '/include.php');
	if(isset($_SESSION['auth'])){
		header("Location: " . getUrl("mainpage.php"));	
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo TITLE; ?></title>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="index.css" type="text/css" /> 
	</head>
	<body>
		<select id="lg">
			<option value="en" <?php if(getLanguage() == "en") { ?>selected<?php } ?>>English</option>
			<option value="fr" <?php if(getLanguage() == "fr") { ?>selected<?php } ?>>Français</option>
		</select> 
	
		<form action="login.php" method="post"> 
			<div class="titre"><?php echo TITLE; ?></div>
		
			<div class="styled-select">
				<table CELLSPACING="5" CELLPADDING="5">
					<tr>
						<td>
							<?php echo WELCOME_MESSAGE; ?>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo LABEL_LOGIN; ?>	
						</td>
					</tr>
					<tr>
						<td>
							<input name="login" maxlength=40 <?php if(isset($_COOKIE["login"])) { ?>value="<?php echo $_COOKIE["login"]; ?>" <?php } ?>/>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo LABEL_PASSWORD; ?>
						</td>
					</tr>
					<tr>
						<td>
							<input type="password" name="password" maxlength=40 />	
						</td>
					</tr>
					<tr>
						<td>
							<INPUT name="Go" type="submit" Value="<?php echo LABEL_SIGN_IN; ?>" width=150> 
						</td>
					</tr>
				</table>
			</div>
		</form>
		<?php
		if(isset($_GET['error'])){ ?>
		<div class="alert alert-danger" role="alert"><?php echo LABEL_LOGERROR ;?></div>
		<?php } ?>
		<script src="<?php echo getUrl("js/libs/jquery.js"); ?>"></script>
		<script src="<?php echo getUrl("js/update_language.js"); ?>"></script>
	</body>
</html>
