<?php
	require(__DIR__ . '/include.php');
	if(!isset($_SESSION['auth'])){
		header("Location: " . getUrl("index.php"));	
		exit();
	}
		if(isset($_GET["tid"])) {
			$tasklist=getTasksList();
			$currenttask = array();
			foreach ($tasklist as $value){
				if ($value[0]==$_GET["tid"]){$currenttask=$value;}
			}
		?>	
<!DOCTYPE html>
<html style="height: 100%">
	<head>
		<title><?php echo TITLE; ?></title>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="admin.css" type="text/css" /> 
	</head>
	<body style="background-image: url('bg_mainpage.png'); background-size: 100% 100%;">
	<div style="width: 100%; text-align: right; margin: 10px;">
			<a href="<?php echo getUrl('mainpage.php'); ?>" style="margin-right: 10px;"><button><?php echo LABEL_TASKS_MANAGEMENT; ?></button></a>
			<a href="<?php echo getUrl('logout.php'); ?>" style="margin-right: 10px;"><button><?php echo LABEL_LOGOUT; ?></button></a>
			<select id="lg" style="margin-right: 10px;">
				<option value="en" <?php if(getLanguage() == "en") { ?>selected<?php } ?>>English</option>
				<option value="fr" <?php if(getLanguage() == "fr") { ?>selected<?php } ?>>Français</option>
			</select> 
		</div>
<div class="container">
	<h2><?php echo LABEL_TASK_EDIT; ?></h2>
	<form action="edit.php" method="post"> 
	<div class="form-group">
		<label for="usr"><?php echo LABEL_TASK_NAME; ?></label>
		<input type="text" class="form-control" id="usr" name="nom" <?php if(isset($currenttask[1])) { ?>value="<?php echo $currenttask[1] ; ?>"<?php } ?>>
	</div>
	<div class="form-group">
		<label for="usr"><?php echo LABEL_TASK_USER; ?></label>
		<select id="author" name="aut">
		<?php
		$loginsList = getUsersList();
		foreach($loginsList as $value) {
			?>
			<option value="<?php echo $value; ?>" <?php if(isset($currenttask[3]) && $currenttask[3] == $value) { ?>selected<?php } ?>><?php echo $value; ?></option>
		<?php } ?>
		</select>
	</div>
	<label for="title"><?php echo LABEL_TASK_STATUS; ?></label>

	<select id="stat" name="status">

 <option value="TODO" <?php if(isset($currenttask[2]) && strtoupper($currenttask[2]) == "TODO") { ?>selected<?php } ?>>To do</option>
 <option value="IN PROGRESS" <?php if(isset($currenttask[2]) && strtoupper($currenttask[2]) == "IN PROGRESS") { ?>selected<?php } ?>>Work in progress</option>
 <option value="DONE" <?php if(isset($currenttask[2]) && strtoupper($currenttask[2]) == "DONE") { ?>selected<?php } ?>>Done</option>

</select>
	
	<input type="text" name="tid" value= <?php echo $currenttask[0]; ?> style="display: none;"/>
	
	<div class="form-group">
		<INPUT name="Go" type="submit" Value="Go" width=150> 
    </div>
	
  </form>
</div>
<script src="<?php echo getUrl("js/libs/jquery.js"); ?>"></script>
<script src="<?php echo getUrl("js/update_language.js"); ?>"></script>
</body>
</html>
		<?php	
		}
		